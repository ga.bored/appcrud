// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDwtJHkDRlLfJSPJNfboqagRhcVaXMYgsE",
    authDomain: "app2-dd558.firebaseapp.com",
    databaseURL: "https://app2-dd558.firebaseio.com",
    projectId: "app2-dd558",
    storageBucket: "app2-dd558.appspot.com",
    messagingSenderId: "512976549802",
    appId: "1:512976549802:web:15219a7aaa6dc1c3fd5dd3"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
